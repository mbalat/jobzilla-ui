import React, { useContext } from 'react'
import { Redirect, Route, Switch } from 'react-router'
import CompaniesList from './components/company/CompaniesList';
import CompanyApplicationsList from './components/company/CompanyApplicationsList';
import CompanyCreate from './components/company/CompanyCreate';
import CompanyJobsList from './components/company/CompanyJobsList';
import CompanyStats from './components/company/CompanyStats';
import AppliedJobsList from './components/job/AppliedJobsList';
import CreateJob from './components/job/CreateJob';
import SingleJob from './components/job/GetSignleJob';
import JobsList from './components/job/JobsList';
import SavedJobsList from './components/job/SavedJobsList';
import OwnersCompaniesList from './components/owner/OwnersCompaniesList';
import UserLogin from './components/user/UserLogin';
import AuthContext from './context/Auth';
import OwnerProfile from './components/owner/OwnerProfile';
import UserProfile from './components/user/UserProfile';
import UserRegister from './components/user/UserRegister';
import CompanyCreateComment from './components/company/CompanyCreateComment';
import UserUpdateProfile from './components/user/UserUpdateProfile';

const Routes: React.FC = () =>
{
    const authContext = useContext(AuthContext);
    
    return(
        <Switch>
        <Redirect exact from ="/" to="/users/login/"/>
        <Route path="/jobs/" exact render={() => authContext.authenticated ? <JobsList /> : <Redirect to={{pathname: '/users/login'}}/>}/>
        <Route path="/user-jobs" exact render={() => authContext.authenticated ? <SavedJobsList /> : <Redirect to={{pathname: '/users/login'}}/>}/>
        <Route path="/applyed-jobs" exact render={() => authContext.authenticated ? <AppliedJobsList /> : <Redirect to={{pathname: '/users/login'}}/>}/>
        <Route path="/companies" exact render={() => authContext.authenticated ? <CompaniesList /> : <Redirect to={{pathname: '/users/login'}}/>}/>
        <Route path="/users/login" exact component={UserLogin}/>
        <Route path="/users/register" exact component={UserRegister}/>
        <Route path="/companies/add" exact render={() => authContext.authenticated ? <CompanyCreate /> : <Redirect to={{pathname: '/users/login'}}/>}/>
        <Route path="/companies/my-companies" exact render={() => authContext.authenticated ? <OwnersCompaniesList /> : <Redirect to={{pathname: '/users/login'}}/>}/>
        <Route path="/jobs/:id" render={() => authContext.authenticated ? <SingleJob /> : <Redirect to={{pathname: '/users/login'}}/>}/>
        <Route path="/companies/:id/stats" render={() => authContext.authenticated ? <CompanyStats /> : <Redirect to={{pathname: '/users/login'}}/>}/>
        <Route path="/companies/:id/create-job" render={() => authContext.authenticated ? <CreateJob /> : <Redirect to={{pathname: '/users/login'}}/>}/>
        <Route path="/companies/:id/applications" render={() => authContext.authenticated ? <CompanyApplicationsList /> : <Redirect to={{pathname: '/users/login'}}/>}/>
        <Route path="/companies/:id/comment" render={() => authContext.authenticated ? <CompanyCreateComment /> : <Redirect to={{pathname: '/users/login'}}/>} />
        <Route path="/companies/:id" render={() => authContext.authenticated ? <CompanyJobsList /> : <Redirect to={{pathname: '/users/login'}}/>}/>
        <Route path="/users/profile/" exact render={() => authContext.authenticated ? <OwnerProfile /> : <Redirect to={{pathname: '/users/login'}}/>}/>
        <Route path="/users/edit-profile" exact render={() => authContext.authenticated ? <UserUpdateProfile /> : <Redirect to={{pathname: '/users/login'}}/>}/>
        <Route path="/users/:id" component={UserProfile}/>
        </Switch>
    );
}

export default Routes;

// /jobs
// /users
// /companies
// /userCompanies
// /userJobs