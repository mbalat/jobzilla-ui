export default interface IUserToken
{
    UserName: string
    Password: string
    Email: string
}