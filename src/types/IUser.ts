export default interface IUserLogin
{
    username: string;
    password: string;
}

export default interface IUserRegister
{
    FullName: string;
    UserName: string;
    Password: string;
    Email: string;
    About: string;
}

export default interface IUserUpdate{
    Email: string;
    About: string;
}