export default interface ICompanyResult
{
    result: {
        Id: string;
        Name: string;
    }
}