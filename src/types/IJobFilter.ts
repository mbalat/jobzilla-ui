export default interface IJobFIlter{
    page?: number,
    rpp?: number,
    searchTerm?: string,
    companyId?: string
}