export default interface IUserJob
{
    id: string;
    userId: string;
    jobId: string;
    isApplyed: boolean;
    isAccepted: boolean;
}