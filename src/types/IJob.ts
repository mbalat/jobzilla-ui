export default interface IJob
{
    Id?: string;
    CompanyId?: string;
    Title: string;
    Description: string;
    Salary: number;
    Location: string;
    DateCreated?: number;
    DateUpdated?: number;
}