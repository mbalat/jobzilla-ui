import IUserLogin from "../types/IUser";
import IUserRegister from "../types/IUser";
import http from "./http";
import IUserUpdate from "../types/IUser";

class UserService
{
    private readonly _baseUrl: string = "/users";
    private readonly _storageKey: string = "token";

    login(user: IUserLogin) {
        return http.post(this._baseUrl + '/login', user);
    }

    register(user: IUserRegister) {
        return http.post(this._baseUrl + '/register', user);
    }

    getProfileInfo() {
        return http.get(this._baseUrl + '/profile', {headers: this.getAuthorization()});
    }

    getUser(id: string) {
        return http.get(this._baseUrl + `/profile/${id}`, {headers: this.getAuthorization()});
    }

    getToken(): string | null {
        return localStorage.getItem(this._storageKey);
    }

    setToken(token: string): void {
        localStorage.setItem(this._storageKey, token);
    }

    removeToken(): void {
        localStorage.removeItem(this._storageKey);
    }

    isAuth(): boolean {
        return this.getToken() != null;
    }

    update(userUpdate: IUserUpdate) {
        return http.put(this._baseUrl + `/edit-profile`, userUpdate, {headers: this.getAuthorization()});
    }

    private getAuthorization() {
        return {
            'Authorization': `Bearer ${this.getToken()}` 
        };
    }
}

export default new UserService();

// POST     /users/register
// POST    /users/login