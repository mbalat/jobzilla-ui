import IUserCompany from "../types/IUSerCompany";
import http from "./http";
import UserService from "./UserService";

class UserCompany {
    private _baseUrl: string = "/user-companies";

    post(companyId: string, userCompany: IUserCompany) {
        return http.post(this._baseUrl + `/${companyId}`, userCompany, {headers: this.getAuthorization()});
    }

    private getAuthorization() {
        return {
            'Authorization': `Bearer ${UserService.getToken()}` 
        };
    }
}

export default new UserCompany();