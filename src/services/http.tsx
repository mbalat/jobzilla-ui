import axios from 'axios';

export default axios.create({
    baseURL: "http://localhost:8000/jobzilla",
    headers: {
        "Content-Type": "application/json",
    }
});