import ICompany from "../types/ICompany";
import IFilter from "../types/IFilter";
import IResultCompany from "../types/IResultCompany";
import http from "./http";
import UserService from "./UserService";

class CompanyService {
    private _baseUrl: string = "/companies";

    find(filter: IFilter) {
        return  http.get(this._baseUrl, {headers: this.getAuthorization(), params: filter});
    }

    post(company: ICompany) {
        return http.post(this._baseUrl, company, {headers: this.getAuthorization()});
    }

    get(id: string) {
        return http.get(this._baseUrl + `/${id}`, {headers: this.getAuthorization()});
    }

    getResult(id: string) {
        return http.get<IResultCompany>(this._baseUrl + `/${id}`, {headers: this.getAuthorization()});
    }

    getMy() {
        return http.get(this._baseUrl + `/my-companies`, {headers: this.getAuthorization()});
    }

    getRate(id: string) {
        return http.get(this._baseUrl + `/${id}/rate`, {headers: this.getAuthorization()});
    }

    getComment(id: string) {
        return http.get(this._baseUrl + `/${id}/comments`, {headers: this.getAuthorization()});
    }

    put(id: string, company: ICompany) {
        return http.put(this._baseUrl + `/${id}`, company, {headers: this.getAuthorization()});
    }

    delete(id: string) {
        return http.delete(this._baseUrl + `/${id}`, {headers: this.getAuthorization()});
    }

    private getAuthorization() {
        return {
            'Authorization': `Bearer ${UserService.getToken()}` 
        };
    }
}

export default new CompanyService();