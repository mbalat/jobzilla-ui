import IJob from "../types/IJob";
import IUserJob from "../types/IUserJob";
import http from "./http";
import UserService from "./UserService";

class UserJob {
    private _baseUrl: string = "/user-jobs";

    find() {
        return http.get(this._baseUrl, {headers: this.getAuthorization()});
    }

    get() {
        return http.get(this._baseUrl + `/applyed-jobs`, {headers: this.getAuthorization()});
    }

    getApplications(companyId: string) {
        return http.get(this._baseUrl + `/${companyId}`, {headers: this.getAuthorization()});
    }

    apply(jobId: string) {
        return http.put(this._baseUrl + `/${jobId}/apply`, null, {headers: this.getAuthorization()});
    }

    accept(id: string) {
        return http.put(this._baseUrl + `/${id}/accept`, null, {headers: this.getAuthorization()});
    }

    deny(id: string) {
        return http.put(this._baseUrl + `/${id}/deny`, null, {headers: this.getAuthorization()});
    }

    post(jobId: string) {
        return http.post(this._baseUrl + `/${jobId}`, null, {headers: this.getAuthorization()});
    }

    delete(jobId: string) {
        return http.delete(this._baseUrl + `/${jobId}`, {headers: this.getAuthorization()});
    }

    private getAuthorization() {
        return {
            'Authorization': `Bearer ${UserService.getToken()}` 
        };
    }
}

export default new UserJob();