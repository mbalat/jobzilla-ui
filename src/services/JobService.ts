import IJobFIlter from "../types/IFilter";
import IJob from "../types/IJob";
import http from "./http";
import UserService from "./UserService";

class JobService {
    private _baseUrl: string = "/jobs";

    find(filter: IJobFIlter) {
        return http.get(this._baseUrl, {headers: this.getAuthorization(), params: filter});
    }

    get(id: string) {
        return http.get(this._baseUrl + `/${id}`, {headers: this.getAuthorization()});
    }

    delete(id: string) {
        return http.delete(this._baseUrl + `/${id}`, {headers: this.getAuthorization()});
    }

    post(job: IJob) {
        return http.post(this._baseUrl, job, {headers: this.getAuthorization()});
    }

    private getAuthorization() {
        return {
            'Authorization': `Bearer ${UserService.getToken()}` 
        };
    }
}

export default new JobService();

// GET     /jobs
// POST    /jobs
// GET     /jobs/:id
// PUT     /jobs/:id
// DELETE  /jobs/:id