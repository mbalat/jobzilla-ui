import { useContext, useEffect, useState } from "react";
import AuthContext from "../../context/Auth";
import UserJobService from "../../services/UserJobService";
import IFilter from "../../types/IFilter";
import { Box, Button, CircularProgress, IconButton, TextField, Typography } from "@material-ui/core";
import Table from "../common/SavedJobsTable";
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import JobService from "../../services/JobService";
import { useHistory } from "react-router-dom";

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const authContext = useContext(AuthContext);
    const [data, setData] = useState();
    const history = useHistory();
    const [count, setTotal] = useState<number>(1);
    const [params, setParams] = useState<IFilter>({page:1});
    const [isLoading, setLoading] = useState<boolean>(true);
    const columns = [
        {title: "Is Applied", rowKey: 'isApplied'},
        {title: "Is Accepted", rowKey: 'isAccepted'},
        {title: "Job Title", rowKey: 'jobTitle'},
        {title: "Company Name", rowKey: 'companyTitle'},
    ]

    const fetchData = async () =>
    {
        setLoading(true);
        let response = await UserJobService.find();
        setData(response.data.items.map((x: any) => {
            return {
                isApplied: x.IsApplyed ? <DoneIcon className="green"/> : <CloseIcon className="red"/>,
                isAccepted: x.IsAccepted ? <DoneIcon className="green"/> : <CloseIcon className="red"/>,
                jobTitle: x.Job.Title,
                companyTitle: x.Job.Company.Name,
                jobId: x.JobId,
            };
        }));
        setTotal(response.data.count);
        setLoading(false);
    }

    useEffect(() => {
        fetchData();
    }, [params.page]);

    const onPageChange = (page: number) =>
    {
        setParams(Object.assign({}, params, {page: page+1}));
    }

    const onSearch = (searchTerm: string) =>
    {
        setParams(Object.assign({}, params, {searchTerm: searchTerm}));
    }

    const onReviewJob = async (id: string) =>
    {
        try {
            const result = await JobService.get(id);
            history.push(`/jobs/${id}`);
            await fetchData();
        } catch (error) {
            console.dir(error);
        }
    }

    const onApply = async (id: string) =>
    {
        try {
            const result = await UserJobService.apply(id);
            await fetchData();
        } catch (error) {
            console.dir(error)
        }
    }

    const onDelete = async (id: string) =>
    {
        try {
            const result = await UserJobService.delete(id);
            await fetchData();
        } catch (error) {
            console.dir(error);
        }
    }

    const actions = Object.assign({}, authContext.authenticated ? { onReviewJob: onReviewJob, onApply: onApply, onDelete: onDelete} : {});

    return(
        <div>
                 
            {!isLoading ?
                <div>
                    <Typography variant="h5"> Saved Jobs</Typography>
                    <Box display="flex" justifyContent="space-between">
                        {authContext.authenticated}
                    </Box>
                    <Table columns={columns} rows={data!} actions={actions} />
                </div> :  <CircularProgress className="loader" color="secondary"/>
            }
        </div>
    )
}