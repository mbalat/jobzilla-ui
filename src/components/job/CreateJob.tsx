import { Button, Grid, TextField, Typography } from "@material-ui/core";
import { useState } from "react";
import { useHistory, useParams } from "react-router-dom"
import JobService from "../../services/JobService";
import IJob from "../../types/IJob";

interface Company{
    id: string
}

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const history = useHistory();
    const { id } = useParams<Company>();
    const [model, setModel] = useState<IJob>();

    const onSubmit = async (e: any) =>
    {
        e.preventDefault();

        if(!model)
            return;

        try {
            const result = await JobService.post(model);
            history.push('/companies/my-companies')
        } catch (error) {
            console.dir(error);
        }
    }

    const setModelValue = (key: string, value: any) =>
    {
        setModel(Object.assign({CompanyId: id}, model, {[key]: value}));
    }

    return(
        <div>
            <Typography variant="h5">Create New Job</Typography>
            <form autoComplete="off" onSubmit={onSubmit} className="f-job">
                <Grid xs={12}>
                    <TextField variant="outlined" id="Title" label="Title" required onChange={e => setModelValue("Title", e.target.value)} />
                </Grid>
                <Grid xs={12}>
                    <TextField variant="outlined" id="Location" label="Location" required onChange={e => setModelValue("Location", e.target.value)} />
                </Grid>
                <Grid xs={12}>
                    <TextField variant="outlined" id="Salary" label="Salary" type="number" required onChange={e => setModelValue("Salary", Number(e.target.value))} />
                </Grid>
                <Grid xs={12}>
                    <TextField variant="outlined" id="Description" label="Description" fullWidth multiline rows={12} required onChange={e => setModelValue("Description", e.target.value)} />
                </Grid>
                <div className="space"></div>
                <Button variant="contained" color="primary" type="submit"> Submit</Button>
            </form>
        </div>
    )
}