import { Box, Button, CircularProgress, IconButton, TextField, Typography } from "@material-ui/core";
import { useContext, useEffect, useState } from "react"
import AuthContext from "../../context/Auth"
import JobService from "../../services/JobService";
import IFilter from "../../types/IFilter";
import Table from "../common/JobTable";
import TablePagination from '../common/TablePagination';
import SearchIcon from '@material-ui/icons/Search';
import { useHistory } from "react-router-dom";
import UserJobService from "../../services/UserJobService";
import moment from "moment";

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const authContext = useContext(AuthContext);
    const [data, setData] = useState();
    const history = useHistory();
    const [total, setTotal] = useState<number>(1);
    const [params, setParams] = useState<IFilter>({page:1});
    const [isLoading, setLoading] = useState<boolean>(true);
    const [active, setActive] = useState(false);
    const [errorMessage, setErrorMessage] = useState<string>();
    const columns = [
        { title: "Title", rowKey: 'title' },
        { title: "Location", rowKey: 'location' },
        { title: "Date Created", rowKey: 'dateCreated' },
        { title: "Salary $", rowKey: 'salary' },
    ];

    const fetchData = async() =>
    {
        setLoading(true);
        let response = await JobService.find(params);
        setData(response.data.items.map((x: any) => {
            return {
                id: x.Id,
                title: x.Title,
                salary: x.Salary,
                location: x.Location,
                dateCreated: moment(x.DateCreated).format("dddd, MMMM Do YYYY")
            };
        }));
        setTotal(response.data.total);
        setLoading(false);
    }

    useEffect(() => {
        fetchData();
    }, [params.page]);

    const onPageChange = (page: number) =>
    {
        setParams(Object.assign({}, params, {page: page+1}));
    }

    const onSearch = (searchTerm: string) =>
    {
        setParams(Object.assign({}, params, {searchTerm: searchTerm}));
    }

    const onSave = async (id: string) =>
    {
        try {
            const result = await UserJobService.post(id);
            await fetchData();
        } catch (error) {
            setActive(true)
            setErrorMessage(error.response.data.message);
        }
    }

    const onApply = async (id: string) =>
    {
        try {
            const result = await UserJobService.apply(id);
            await fetchData();
        } catch (error) {
            setActive(true)
            setErrorMessage(error.response.data.message);
        }
    }

    const onView = async (id: string) =>
    {
        try {
            const result = await JobService.get(id);
            history.push(`/jobs/${id}`);
            await(fetchData);
        } catch (error) {
            console.dir(error)
        }
    }

    const actions = Object.assign({}, authContext.authenticated ? {onSave: onSave, onApply: onApply, onView: onView} : {});

    return(
        <div>
            {!isLoading ?
                <div>
                    <Typography variant="h5"> Jobs</Typography>
                    <Box display="flex" justifyContent="space-between">
                        <Box>
                            <TextField id="standard-search" label="Search field" type="search" value={params.searchTerm} onChange={e => onSearch(e.target.value)} />
                            <IconButton aria-label="Search" onClick={() => {fetchData()}}>
                                <SearchIcon />
                            </IconButton>
                        </Box>
                        {authContext.authenticated && <Button variant="outlined" color="primary" href="/companies/my-companies">Add new job</Button>}
                    </Box>
                    <Table columns={columns} rows={data!} actions={actions} />
                    <TablePagination page={params.page! - 1} rpp={10} count={total} onChange={onPageChange} />
                    <div className={active ? "modal" : "disabled"}>
                        <div className="modal-content">
                            <div className="w-m">{errorMessage}</div>
                            <div className="w-b">
                                <Button variant="contained" color="primary" onClick={() => setActive(!active)}> Close</Button>
                            </div>
                        </div>
                </div>
                </div> :  <CircularProgress className="loader" color="secondary"/>
            }
        </div>
    )
}