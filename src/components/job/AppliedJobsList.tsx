import React, { useContext, useEffect, useState } from "react";
import AuthContext from "../../context/Auth";
import UserJobService from "../../services/UserJobService";
import IFilter from "../../types/IFilter";
import { Box, Button, CircularProgress, IconButton, TextField, Typography } from "@material-ui/core";
import Table from "../common/AppliedJobsTable";
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import JobService from "../../services/JobService";
import { useHistory } from "react-router-dom";

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const authContext = useContext(AuthContext);
    const [data, setData] = useState();
    const history = useHistory();
    const [isLoading, setLoading] = useState<boolean>(true);
    const columns = [
        { title: "Is Applied", rowKey: 'isApplied' },
        { title: "Is Accepted", rowKey: 'isAccepted'},
        { title: "Job Title", rowKey: 'jobTitle'}
    ];

    const fetchData = async () =>
    {
        setLoading(true);
        let response = await UserJobService.get();
        setData(response.data.items.map((x: any) => {
            return {
                isApplied: x.IsApplyed ? <DoneIcon className="green"/> : <CloseIcon className="red"/>,
                isAccepted: x.IsAccepted ? <DoneIcon className="green"/> : <CloseIcon className="red"/>,
                jobTitle: x.Job.Title,
                jobId: x.JobId,
                companyId: x.Job.CompanyId
            };
        }));
        setLoading(false);
    }

    useEffect(() => {
        fetchData();
    }, []);

    const onReviewJob = async (id: string) =>
    {
        try {
            const result = await JobService.get(id);
            history.push(`/jobs/${id}`);
            await fetchData();
        } catch (error) {
            console.dir(error);
        }
    }

    const onDelete = async (id: string) =>
    {
        try {
            const result = await UserJobService.delete(id);
            await fetchData();
        } catch (error) {
            console.dir(error);
        }
    }

    const onComment = async (id: string) =>
    {
        try {
            history.push(`/companies/${id}/comment`);
        } catch (error) {
            console.dir(error)
        }
    }

    const actions = Object.assign({}, authContext.authenticated ? { onReviewJob: onReviewJob, onDelete: onDelete, onComment: onComment} : {});

    return(
        <div>
                 
            {!isLoading ?
                <div>
                    <Typography variant="h5"> Applied Jobs</Typography>
                    <Box display="flex" justifyContent="space-between">
                        {authContext.authenticated}
                    </Box>
                    <Table columns={columns} rows={data!} actions={actions!} />
                </div> :  <CircularProgress className="loader" color="secondary"/>
            }
        </div>
    )
}