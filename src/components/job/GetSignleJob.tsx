import { Card, CardActionArea, CardContent, CircularProgress, Typography} from "@material-ui/core";
import { useEffect } from "react";
import { useContext, useState } from "react";
import { useParams } from "react-router-dom";
import AuthContext from "../../context/Auth";
import CompanyService from "../../services/CompanyService";
import JobService from "../../services/JobService";
import ICompany from "../../types/ICompany";

interface Job {
    id?: string;
}

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const authContext = useContext(AuthContext);
    const { id } = useParams<Job>();
    const [isLoading, setLoading] = useState<boolean>(true);
    const [title, setTitle] = useState<string>();
    const [salary, setSalary] = useState<string>();
    const [location, setLocation] = useState<string>();
    const [companyId, setCompanyId] = useState<string>();
    const [description, setDescription] = useState<string>();
    const [company, setCompany] = useState<string | undefined>();

    const fetchData = async () =>
    {
        setLoading(true);
        debugger;
        let response = await JobService.get(id!);
        let companyResponse = await CompanyService.getResult(response.data.CompanyId!);
        
        setTitle(response.data.Title);
        setSalary(response.data.Salary);
        setLocation(response.data.Location);
        setCompanyId(response.data.CompanyId);
        setDescription(response.data.Description);
        setCompany(companyResponse.data.result.Name);
        setLoading(false);
    }

    useEffect(() => {
        fetchData();
    }, []);

    return(
        <div>
            {!isLoading ? 
               <Card>
                 <CardContent>
                   <Typography gutterBottom variant="h5" component="h2">
                     {title}
                   </Typography>
                   <Typography gutterBottom variant="caption" component="h6">
                     Location: {location}
                   </Typography>
                   <Typography gutterBottom variant="caption" component="h6">
                     Salary: {salary} $
                   </Typography>
                   <Typography gutterBottom variant="caption" component="h6">
                     Company: {company}
                   </Typography>
                   <Typography gutterBottom variant="caption" component="h6">
                     Description:
                   </Typography>
                   <Typography variant="body2" color="textSecondary" component="p">
                     {description}
                   </Typography>
                 </CardContent>
             </Card>
                 : <CircularProgress className="loader" color="secondary"/>
            }
        </div>
    )
}