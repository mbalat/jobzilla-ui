import { TableContainer, Table, TableHead, TableRow, TableCell, Typography, TableBody, IconButton, Button } from "@material-ui/core";
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';


interface IDictionary {
    [Key: string]: any;
}

interface ITableAction{
    onReview?: (id: string) => void
    onDelete?: (id: string) => void
    onGetCompanyJobs?: (id: string) => void
    onAddJob?: (id: string) => void
    onGetApplications?: (id: string) => void
}

interface ColumnOption{
    title: string;
    rowKey: string;
}

interface ITableProps
{
    columns: ColumnOption[],
    rows: IDictionary[],
    actions?: ITableAction
}

// eslint-disable-next-line import/no-anonymous-default-export
export default ({columns, rows, actions}: ITableProps) => 
{
    return(
        <TableContainer>
            <Table stickyHeader size="small">
                <TableHead>
                <TableRow>
                        {columns.map(column => (
                            <TableCell key={column.rowKey}>
                                <Typography variant="h6">{column.title}</Typography>
                            </TableCell>
                        ))}
                        {
                            actions && <TableCell>Actions</TableCell>
                        }
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.id}>
                            {
                                columns.map(c => {
                                    return <TableCell key={c.rowKey+row.id} align="left">{row[c.rowKey]}</TableCell>
                                })

                            }
                            {
                                actions && actions.onReview && actions.onDelete && actions.onGetCompanyJobs && actions.onAddJob &&
                                <TableCell>
                                    <IconButton title="View" onClick={() => actions.onReview!(row.id)}>
                                        <VisibilityIcon />
                                    </IconButton>
                                    <IconButton title="Delete" onClick={() => actions.onDelete!(row.id)}>
                                        <DeleteIcon />
                                    </IconButton>
                                    <span className="m-r-2">
                                        <Button variant="outlined" color="primary" onClick={() => actions.onGetCompanyJobs!(row.id)}>Jobs</Button>
                                    </span>
                                    <span className="m-r-2">
                                        <Button variant="outlined" color="primary" onClick={() => actions.onAddJob!(row.id)}>Add Job</Button>
                                    </span>
                                    <span className="m-r-2">
                                        <Button variant="outlined" color="primary" onClick={() => actions.onGetApplications!(row.id)}>Applications</Button>
                                    </span>
                                </TableCell>
                            }
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}