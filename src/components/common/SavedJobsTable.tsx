import { TableContainer, Table, TableHead, TableRow, TableCell, Typography, TableBody, IconButton } from "@material-ui/core";
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';
import BookmarkIcon from '@material-ui/icons/Bookmark';

interface IDictionary {
    [Key: string]: any;
}

interface ITableAction{
    onReviewJob?: (id: string) => void
    onApply?: (id: string) => void
    onDelete?: (id: string) => void
}

interface ColumnOption{
    title: string;
    rowKey: string;
}

interface ITableProps
{
    columns: ColumnOption[],
    rows: IDictionary[],
    actions?: ITableAction
}

// eslint-disable-next-line import/no-anonymous-default-export
export default ({columns, rows, actions}: ITableProps) => 
{
    return(
        <TableContainer>
            <Table stickyHeader size="small">
                <TableHead>
                    <TableRow>
                            {columns.map(column => (
                                <TableCell key={column.rowKey}>
                                    <Typography variant="h6">{column.title}</Typography>
                                </TableCell>
                            ))}
                            {
                                actions && <TableCell>Actions</TableCell>
                            }
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((row) => (
                            <TableRow key={row.id}>
                                {
                                    columns.map(c => {
                                        return <TableCell key={c.rowKey+row.id} align="left">{row[c.rowKey]}</TableCell>
                                    })

                                }
                                {
                                actions && actions.onReviewJob && actions.onApply && actions.onDelete &&
                                <TableCell>
                                    <IconButton title="View" onClick={() => actions.onReviewJob!(row.jobId)}>  
                                        <VisibilityIcon />
                                    </IconButton>
                                    <IconButton title="Apply" onClick={() => actions.onApply!(row.jobId)}>
                                        <BookmarkIcon/>
                                    </IconButton>
                                    <IconButton title="Delete">
                                        <DeleteIcon onClick={() => actions.onDelete!(row.jobId)}/>
                                    </IconButton>
                                </TableCell>
                            }
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}