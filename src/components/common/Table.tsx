import { TableContainer, Table, TableHead, TableRow, TableCell, Typography, TableBody, IconButton } from "@material-ui/core";
import DeleteOutlineTwoToneIcon from '@material-ui/icons/DeleteOutlineTwoTone';

interface IDictionary {
    [Key: string]: any;
}

interface ITableAction{
    onDelete?: (id: string) => void
}

interface ITableProps
{
    columns: string[],
    rows: IDictionary[],
    actions?: ITableAction
}

// eslint-disable-next-line import/no-anonymous-default-export
export default ({columns, rows, actions}: ITableProps) => 
{
    return(
        <TableContainer>
            <Table stickyHeader size="small">
                <TableHead>
                    <TableRow>
                        {columns.map(column => (
                            <TableCell key={column}>
                                <Typography variant="h6">{column}</Typography>
                            </TableCell>
                        ))}
                        {
                            actions && actions.onDelete && <TableCell>Actions</TableCell>
                        }
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.id}>
                            {
                                Object.entries(row).map(([key, value]) => {
                                    return <TableCell key={key} align="left">{value}</TableCell>
                                })
                            }
                            {
                                actions && actions.onDelete &&
                                <TableCell>
                                    <IconButton aria-label="Delete" onClick={() => actions.onDelete!(row.id)}>
                                        <DeleteOutlineTwoToneIcon />
                                    </IconButton>
                                </TableCell>
                            }
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
};