import { TableContainer, Table, TableHead, TableRow, TableCell, Typography, TableBody, IconButton } from "@material-ui/core";
import VisibilityIcon from '@material-ui/icons/Visibility';

interface IDictionary {
    [Key: string]: any;
}

interface ITableAction{
    onReviewCompany?: (id: string) => void
}

interface ColumnOption{
    title: string;
    rowKey: string;
}

interface ITableProps
{
    columns: ColumnOption[],
    rows: IDictionary[],
    actions?: ITableAction
}

// eslint-disable-next-line import/no-anonymous-default-export
export default ({columns, rows, actions}: ITableProps) => 
{
    return(
        <TableContainer>
            <Table stickyHeader size="small">
                <TableHead>
                    <TableRow>
                            {columns.map(column => (
                                <TableCell key={column.rowKey}>
                                    <Typography variant="h6">{column.title}</Typography>
                                </TableCell>
                            ))}
                            {
                                actions && <TableCell>Actions</TableCell>
                            }
                        </TableRow>
                    </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.id}>
                            {
                                columns.map(c => {
                                    return <TableCell key={c.rowKey+row.id} align="left">{row[c.rowKey]}</TableCell>
                                })

                            }
                            {
                                actions && actions.onReviewCompany &&
                                <TableCell>
                                    <IconButton title="View" onClick={() => actions.onReviewCompany!(row.id)}>
                                        <VisibilityIcon />
                                    </IconButton>
                                </TableCell>
                            }
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}