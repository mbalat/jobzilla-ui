import { AppBar, Box, Button, IconButton, Toolbar, Typography } from "@material-ui/core";
import { useContext } from "react"
import AuthContext from "../../context/Auth"
import UserService from "../../services/UserService";
import DashboardIcon from '@material-ui/icons/Dashboard';
import SaveIcon from '@material-ui/icons/Save';
import BookmarkIcon from '@material-ui/icons/Bookmark';
import BusinessIcon from '@material-ui/icons/Business';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { useHistory } from "react-router-dom";

// eslint-disable-next-line import/no-anonymous-default-export
export default () => 
{
    const authContext = useContext(AuthContext);
    const history = useHistory();
    

    const logout = () =>
    {
        UserService.removeToken();
        authContext.setAuthenticated(false);
        history.push("/users/login");
    }

    return(
        <AppBar position="static">
            <Toolbar>
                <Box width={1} display="flex" justifyContent="space-between">
                    <IconButton title="Dashboard" href="/jobs">
                        <DashboardIcon className="icon-white"/>
                    </IconButton>
                    <IconButton title="Saved" href="/user-jobs">
                        <SaveIcon className="icon-white" />
                    </IconButton>
                    <IconButton title="Applied" href="/applyed-jobs">
                        <BookmarkIcon className="icon-white" />
                    </IconButton>
                    <IconButton title="Companies" href="/companies">
                        <BusinessIcon className="icon-white" />
                    </IconButton>
                        <Box width={0.6} display="flex" justifyContent="space-between">
                        <Typography variant="h3"> JobZilla</Typography>
                        <IconButton title="My Companies" href="/companies/my-companies">
                            <DashboardIcon className="icon-profile"/>
                        </IconButton>
                        <IconButton title="My Profile" href="/users/profile">
                            <AccountCircleIcon className="icon-profile" />
                        </IconButton>
                        {
                            authContext.authenticated ?
                            <Button color="inherit" onClick={() => logout()}>Logout</Button> :
                            <Button color="inherit" href="/users/login">Login</Button>
                        }
                        </Box>
                    </Box>
            </Toolbar>
        </AppBar>
    )
}