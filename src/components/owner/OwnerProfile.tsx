import { Button, Card, CardActionArea, CardContent, CircularProgress, Typography} from "@material-ui/core";
import { useEffect } from "react";
import { useContext, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import AuthContext from "../../context/Auth";
import UserService from "../../services/UserService";

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const authContext = useContext(AuthContext);
    const history = useHistory();
    const [isLoading, setLoading] = useState<boolean>(true);
    const [fullName, setFullName] = useState<string>();
    const [userName, setUserName] = useState<string>();
    const [email, setEmail] = useState<string>();
    const [about, setAbout] = useState<string>();

    const fetchData = async () =>
    {
        setLoading(true);
        let response = await UserService.getProfileInfo();
        setFullName(response.data.FullName);
        setUserName(response.data.UserName);
        setEmail(response.data.Email);
        setAbout(response.data.About);
        setLoading(false);
    }

    useEffect(() => {
        fetchData();
    }, []);

    const onGetEdit = () => {
      history.push(`/users/edit-profile`);
    }

    return(
        <div>
            {!isLoading ? 
               <Card>
                 <CardContent>
                   <Typography gutterBottom variant="h4" component="h2">
                     Welcome {fullName}
                   </Typography>
                   <Typography gutterBottom variant="h6" component="h6">
                     Username: {userName}
                   </Typography>
                   <Typography gutterBottom variant="h6" component="h6">
                     Email: {email}
                   </Typography>
                   <Typography gutterBottom variant="h6" component="h6">
                     About:
                   </Typography>
                   <Typography variant="body1" color="textSecondary" component="p">
                     {about}
                   </Typography>
                   <div className="space"></div>
                 <Button className="button-margin-top" variant="contained" color="primary" onClick={onGetEdit}>
                        Edit Info
                    </Button>
                 </CardContent>
             </Card>
                 : <CircularProgress className="loader" color="secondary"/>
            }
        </div>
    )
}