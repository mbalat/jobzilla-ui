import { Box, Button, CircularProgress, IconButton, TextField, Typography } from "@material-ui/core";
import { useEffect } from "react";
import { useState } from "react";
import { useContext } from "react"
import { useHistory } from "react-router-dom";
import AuthContext from "../../context/Auth"
import CompanyService from "../../services/CompanyService";
import JobService from "../../services/JobService";
import Table from '../common/OwnerCompaniesTable';
import IJobFilter from "../../types/IJobFilter";

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const authContext = useContext(AuthContext);
    const [data, setData] = useState();
    const history = useHistory();
    const [isLoading, setLoading] = useState<boolean>(true);
    const [params, setParams] = useState<IJobFilter>();
    const columns = [
        { title: "Company Name", rowKey: "name"}
    ];

    const fetchData = async () =>
    {
        setLoading(true);
        let response = await CompanyService.getMy();
        setData(response.data.map((x: any) => {
            return {
                id: x.Id,
                name: x.Name
            }
        }));
        setLoading(false);
    }

    useEffect(() => {
        fetchData();
    }, [params?.companyId]);

    const onCompanyId = (id: string) =>
    {
       return setParams(Object.assign({}, params, {companyId: id}));
    }

    const onReview = async (id: string) =>
    {
        try {   
            const result = await CompanyService.get(id);
            history.push(`/companies/${id}/stats`);
            await fetchData();
        } catch (error) {
            console.dir(error)
        }
    }

    const onDelete = async (id: string) =>
    {
        try {
            const result = await CompanyService.delete(id);
            await fetchData();
        } catch (error) {
            console.dir(error);
        }
    }

    const onGetCompanyJobs = async (id: string) =>
    {
        try {
            const result = await JobService.find(onCompanyId(id)!);
            history.push(`/companies/${id}`);
            await fetchData();
        } catch (error) {
            console.dir(error);
        }
    }

    const onAddJob = async (id: string) =>
    {
        try {
            history.push(`/companies/${id}/create-job`);
            await fetchData();
        } catch (error) {
            console.dir(error);
        }
    }

    const onGetApplications = async (id: string) =>
    {
        try {
            history.push(`/companies/${id}/applications`);
            await fetchData();
        } catch (error) {
            console.dir(error);
        }
    }
    

    const actions = Object.assign({}, authContext.authenticated ? { onReview: onReview, onDelete: onDelete, onGetCompanyJobs: onGetCompanyJobs, onAddJob: onAddJob, onGetApplications: onGetApplications} : {});

    return(
        <div>
                 
            {!isLoading ?
                <div>
                    <Typography variant="h5"> Your Companies</Typography>
                    <Box display="flex" justifyContent="space-between">
                        {authContext.authenticated}
                    <div></div>
                    {authContext.authenticated && <Button variant="outlined" color="primary" href="/companies/add">Add new company</Button>}
                    </Box>
                    <Table columns={columns} rows={data!} actions={actions!} />
                </div> :  <CircularProgress className="loader" color="secondary"/>
            }
        </div>
    )
}