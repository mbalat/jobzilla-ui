import { Box, Button, CircularProgress, IconButton, TextField, Typography } from "@material-ui/core";
import { useContext } from "react";
import moment from "moment";
import { useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import AuthContext from "../../context/Auth";
import TablePagination from '../common/TablePagination';
import SearchIcon from '@material-ui/icons/Search';
import Table from '../common/CompanyJobsTable';
import { useState } from "react";
import JobService from "../../services/JobService";
import IJobFilter from "../../types/IJobFilter";

interface Company{
    id: string
}

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const authContext = useContext(AuthContext);
    const [data, setData] = useState();
    const history = useHistory();
    const { id } = useParams<Company>();
    const [total, setTotal] = useState<number>(1);
    const [params, setParams] = useState<IJobFilter>({companyId: id, page:1});
    const [isLoading, setLoading] = useState<boolean>(true);
    const columns = [
        { title: "Title", rowKey: 'title' },
        { title: "Location", rowKey: 'location' },
        { title: "Date Created", rowKey: 'dateCreated' },
        { title: "Salary $", rowKey: 'salary' },
    ];

    const fetchData = async () =>
    {
        setLoading(true);
        let response = await JobService.find(params);
        setData(response.data.items.map((x: any) => {
            return {
                id: x.Id,
                title: x.Title,
                salary: x.Salary,
                location: x.Location,
                dateCreated: moment(x.DateCreated).format("dddd, MMMM Do YYYY")
            }
        }));
        setTotal(response.data.total);
        setLoading(false);
    }

    useEffect(() => {
        fetchData();
    }, [params]);

    const onPageChange = (page: number) =>
    {
        setParams(Object.assign({}, params, {page: page+1}));
    }

    const onSearch = (searchTerm: string) =>
    {
        setParams(Object.assign({}, params, {searchTerm: searchTerm}));
    }

    const onReview = async (id: string) =>
    {
        try {
            const result = await JobService.get(id);
            history.push(`/jobs/${id}`);
            await(fetchData);
        } catch (error) {
            console.dir(error)
        }
    }

    const actions = Object.assign({}, authContext.authenticated ? {onReview: onReview} : {});

    return(
        <div>
            {!isLoading ?
                <div>
                    <Typography variant="h5"> Jobs</Typography>
                    <Box display="flex" justifyContent="space-between">
                        <Box>
                            <TextField id="standard-search" label="Search field" type="search" value={params.searchTerm} onChange={e => onSearch(e.target.value)} />
                            <IconButton aria-label="Search" onClick={() => {fetchData()}}>
                                <SearchIcon />
                            </IconButton>
                        </Box>
                        {authContext.authenticated && <Button variant="outlined" color="primary" href="/companies/my-companies">Add new job</Button>}
                    </Box>
                    <Table columns={columns} rows={data!} actions={actions} />
                    <TablePagination page={params.page! - 1} rpp={10} count={total} onChange={onPageChange} />
                </div> :  <CircularProgress className="loader" color="secondary"/>
            }
        </div>
    )
}