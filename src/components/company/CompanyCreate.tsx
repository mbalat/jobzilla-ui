import { Button, Grid, TextField, Typography } from "@material-ui/core";
import { useState } from "react";
import { useHistory } from "react-router-dom"
import CompanyService from "../../services/CompanyService";
import ICompany from "../../types/ICompany";

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const history = useHistory();
    const [model, setModel] = useState<ICompany>();

    const onSubmit = async (e: any) => 
    {
        e.preventDefault();

        if(!model)
            return;

        try {
            const result = await CompanyService.post(model);
            history.push('/companies/my-companies')
        } catch (error) {
            console.dir(error);
        }
    }

    const setModelValue = (key: string, value: any) =>
    {
        setModel(Object.assign({}, model, {[key]: value}));
    }

    return (
        <div>
            <Typography variant="h5">Create new Company</Typography>
            <form autoComplete="off" onSubmit={onSubmit}>
                <Grid xs={12}>
                    <TextField id="Name" label="Company Name" required onChange={e => setModelValue("Name", e.target.value)} />
                </Grid>
                <div className="space"></div>
                <Button variant="contained" color="primary" type="submit"> Submit</Button>
            </form>
        </div>
    );
}