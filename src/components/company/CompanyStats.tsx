import { Button, Card, CardActionArea, CardContent, CircularProgress, Typography} from "@material-ui/core";
import moment from "moment";
import { useEffect } from "react";
import { useContext, useState } from "react"
import { useParams } from "react-router-dom";
import AuthContext from "../../context/Auth"
import CompanyService from "../../services/CompanyService";

interface Company{
    id: string;
}

interface User {
    UserName: string;
}

interface Comment {
    Comment: string;
    Rate: number;
    DateCreated: number;
    User: User;
}

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const authContext = useContext(AuthContext);
    const { id } = useParams<Company>();
    const [isLoading, setLoading] = useState<boolean>();
    const [name, setName] = useState<string>();
    const [userId, setUserId] = useState<string>();
    const [rate, setRate] = useState<number>();
    const [comments, setComments] = useState<[Comment]>();
    const [active, setActive] = useState(false);
    const [errorMessage, setErrorMessage] = useState<string>();

    const fetchData = async () =>
    {
        setLoading(true);
        let companyResponse = await CompanyService.get(id!);
        let companyCommentsResponse = await CompanyService.getComment(id!);
        
        setName(companyResponse.data.result.Name);
        setUserId(companyResponse.data.result.UserId);
        setComments(companyCommentsResponse.data);
        setLoading(false);
        try{
            let companyRateResponse = await CompanyService.getRate(id!);
            setRate(companyRateResponse.data.AvgRate);
        } catch (error) {
            setActive(true)
            setErrorMessage(error.response.data.message);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return(
        <div>
            {!isLoading ? 
                <div className="margin-top">
                    <Card>
                        <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {name}
                        </Typography>
                        <Typography gutterBottom variant="caption" component="h6">
                            Rate: {rate}
                        </Typography>
                        </CardContent>
                    </Card>
                    {comments?.map((comment) => {
                        return(
                            <div className="comment-border">
                                <Card className="blue-background">
                                    <CardActionArea>
                                        <CardContent>
                                            <div className="comment-head">
                                                <div className="comment-user">
                                                    {comment.User.UserName}
                                                </div>
                                                <div className="comment-date">
                                                    @ {moment(comment.DateCreated).format("dddd, MMMM Do YYYY")}
                                                </div>
                                            </div>
                                            <div className="comment-rate">
                                                Rate: {comment.Rate > 2 ? <div className="green-rate">{comment.Rate}</div> : <div className="red-rate">{comment.Rate}</div>}
                                            </div>
                                            <div>
                                                <div className="comment-message">
                                                    {comment.Comment}
                                                </div>
                                            </div>
                                        </CardContent>
                                    </CardActionArea>
                                </Card>
                            </div>
                        )
                    })}
                    <div className={active ? "modal" : "disabled"}>
                <div className="modal-content">
                    <div className="w-m">{errorMessage}</div>
                    <div className="w-b">
                        <Button variant="contained" color="primary" onClick={() => setActive(!active)}> Close</Button>
                    </div>
                </div>
            </div>
             </div>

                 :  <CircularProgress className="loader" color="secondary"/>
            }
        </div>
    )
}