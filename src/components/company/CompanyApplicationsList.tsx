import { Box, Button, CircularProgress, IconButton, TextField, Typography } from "@material-ui/core";
import { useEffect, useState } from "react";
import { useContext } from "react";
import { useHistory, useParams } from "react-router-dom";
import AuthContext from "../../context/Auth"
import UserJobService from "../../services/UserJobService";
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import Table from '../common/CompanyApplicationsTable';
import Link from '@material-ui/core/Link';

interface Company{
    id: string;
}


// eslint-disable-next-line import/no-anonymous-default-export
export default () => {
    const authContext = useContext(AuthContext);
    const [data, setData] = useState();
    const history = useHistory();
    const { id } = useParams<Company>();
    const [isLoading, setLoading] = useState<boolean>(true);
    const columns = [
        { title: "Is Accepted", rowKey: 'isAccepted'},
        { title: "Job Title", rowKey: 'jobTitle'},
        { title: "User", rowKey: 'user'}
    ];

    const fetchData = async () =>
    {
        setLoading(true);
        let response = await UserJobService.getApplications(id);
        setData(response.data.map((x: any) => {
            return {
                id: x.Id,
                isAccepted: x.IsAccepted ? <DoneIcon className="green"/> : <CloseIcon className="red"/>,
                jobTitle: x.Job.Title,
                user: <Link onClick={() => {history.push(`/users/${x.User.Id}`)}} >{x.User.UserName}</Link>
            }
        }));
        setLoading(false);
    }

    useEffect(() => {
        fetchData();
    }, []);

    const onAccept = async (id: string) =>
    {
        try {
            const result = await UserJobService.accept(id);
            await fetchData();
        } catch (error) {
            console.dir(error);
        }
    };

    const onDeny = async (id: string) => {
        try {
            const result = await UserJobService.deny(id);
            await fetchData();
        } catch (error) {
            console.dir(error);
        }
    };

    const actions = Object.assign({}, authContext.authenticated ? {onAccept: onAccept, onDeny: onDeny} : {});

    return(
        <div>
                 
            {!isLoading ?
                <div>
                    <Typography variant="h5"> Applications</Typography>
                    <Box display="flex" justifyContent="space-between">
                        {authContext.authenticated}
                    </Box>
                    <Table columns={columns} rows={data!} actions={actions!} />
                </div> :  <CircularProgress className="loader" color="secondary"/>
            }
        </div>
    )
}