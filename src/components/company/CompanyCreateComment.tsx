import { Box, Button, CardContent, FormControl, FormHelperText, Grid, InputLabel, MenuItem, Select, TextField, Typography } from "@material-ui/core";
import { useHistory, useParams } from "react-router-dom";
import React, { useContext, useEffect, useState } from "react";
import IUserCompany from "../../types/IUSerCompany";
import UserCompanyService from "../../services/UserCompanyService";


interface Company{
    id: string
}

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const history = useHistory();
    const { id } = useParams<Company>();
    const [model, setModel] = useState<IUserCompany>();
    const [active, setActive] = useState(false);
    const [errorMessage, setErrorMessage] = useState<string>();

    const onSubmit = async (e: any) =>
    {
        e.preventDefault();

        if(!model)
            return;

        try {
            const result = await UserCompanyService.post(id, model);
            history.push(`/companies/${id}/stats`);
        } catch (error) {
            setActive(true)
            setErrorMessage(error.response.data[0]);
        }
    }

    const setModelValue = (key: string, value: any) =>
    {
        setModel(Object.assign({}, model, {[key]: value}));
    }

    return(
        <div>
            <Typography variant="h5">Comment</Typography>
            <form autoComplete="off" onSubmit={onSubmit} className="f-job">
                <Grid xs={12}>
                    <TextField variant="outlined" id="Rate" label="Rate" type="number" required onChange={e => setModelValue("Rate", Number(e.target.value))} />
                </Grid>
                <Box component="div">Rate between 1 and 5</Box>
                <Grid xs={12}>
                    <TextField variant="outlined" id="Comment" label="Comment" fullWidth multiline rows={12} required onChange={e => setModelValue("Comment", e.target.value)} />
                </Grid>
                <div className="space"></div>
                <Button variant="contained" color="primary" type="submit"> Submit</Button>
            </form>
            <div className={active ? "modal" : "disabled"}>
                <div className="modal-content">
                    <div className="w-m">{errorMessage}</div>
                    <div className="w-b">
                        <Button variant="contained" color="primary" onClick={() => setActive(!active)}> Close</Button>
                    </div>
                </div>
            </div>
        </div>
    )
}