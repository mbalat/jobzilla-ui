import React, { useContext, useEffect, useState } from "react"
import AuthContext from "../../context/Auth"
import CompanyService from "../../services/CompanyService";
import IFilter from "../../types/IFilter";
import { Box, Button, CircularProgress, IconButton, TextField, Typography } from "@material-ui/core";
import Table from "../common/CompanyTable";
import SearchIcon from '@material-ui/icons/Search';
import TablePagination from '../common/TablePagination';
import { useHistory } from "react-router-dom";

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const authContext = useContext(AuthContext);
    const [data, setData] = useState();
    const history = useHistory();
    const [count, setTotal] = useState<number>(1);
    const [params, setParams] = useState<IFilter>({page:1});
    const [isLoading, setLoading] = useState<boolean>(true);
    const columns = [
        { title: "Name", rowKey: 'name' },
        { title: "Owner", rowKey: 'owner' },
    ];

    const fetchData = async () =>
    {
        setLoading(true);
        let response = await CompanyService.find(params);
        setData(response.data.rows.map((x: any) => {
            return {
                id: x.Id,
                name: x.Name,
                owner: x.User.UserName
            };
        }));
        setTotal(response.data.count);
        setLoading(false);
    }

    useEffect(() => {
        fetchData();
    }, [params.page]);

    const onPageChange = (page: number) =>
    {
        setParams(Object.assign({}, params, {page: page+1}));
    }

    const onSearch = (searchTerm: string) =>
    {
        setParams(Object.assign({}, params, {searchTerm: searchTerm}));
    }

    const onReviewCompany = async (id: string) =>
    {
        try {
            const result = await CompanyService.get(id);
            history.push(`/companies/${id}/stats`);
            await fetchData();
        } catch (error) {
            console.dir(error);
        }
    }

    const actions = Object.assign({}, authContext.authenticated ? { onReviewCompany: onReviewCompany} : {});

    return(
        <div>
                 
            {!isLoading ?
                <div>
                    <Typography variant="h5"> Companies</Typography>
                    <Box display="flex" justifyContent="space-between">
                        <Box>
                            <TextField id="standard-search" label="Search field" type="search" value={params.searchTerm} onChange={e => onSearch(e.target.value)} />
                            <IconButton aria-label="Search" onClick={() => {fetchData()}}>
                                <SearchIcon />
                            </IconButton>
                        </Box>
                        {authContext.authenticated && <Button variant="outlined" color="primary" href="/companies/add">Add new company</Button>}
                    </Box>
                    <Table columns={columns} rows={data!} actions={actions} />
                    <TablePagination page={params.page! - 1} rpp={10} count={count} onChange={onPageChange} />
                </div> :  <CircularProgress className="loader" color="secondary"/>
            }
        </div>
    )
}