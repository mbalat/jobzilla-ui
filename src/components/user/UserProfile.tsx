import { Button, Card, CardActionArea, CardContent, CircularProgress, Typography} from "@material-ui/core";
import { useEffect } from "react";
import { useContext, useState } from "react";
import { useParams } from "react-router-dom";
import AuthContext from "../../context/Auth";
import UserService from "../../services/UserService";

interface User {
    id?: string;
}

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const authContext = useContext(AuthContext);
    const { id } = useParams<User>();
    const [isLoading, setLoading] = useState<boolean>(true);
    const [fullName, setFullName] = useState<string>();
    const [userName, setUserName] = useState<string>();
    const [email, setEmail] = useState<string>();
    const [about, setAbout] = useState<string>();

    const fetchData = async () =>
    {
        setLoading(true);
        let response = await UserService.getUser(id!);
        setFullName(response.data.FullName);
        setUserName(response.data.UserName);
        setEmail(response.data.Email);
        setAbout(response.data.About);
        setLoading(false);
    }

    useEffect(() => {
        fetchData();
    }, []);

    return(
        <div>
            {!isLoading ? 
               <Card>
                 <CardContent>
                   <Typography gutterBottom variant="h6" component="h6">
                     Username: {userName}
                   </Typography>
                   <Typography gutterBottom variant="h6" component="h6">
                     Email: {email}
                   </Typography>
                   <Typography gutterBottom variant="h6" component="h6">
                     About:
                   </Typography>
                   <Typography variant="body1" color="textSecondary" component="p">
                     {about}
                   </Typography>
                   <div className="space"></div>
                 </CardContent>
             </Card>
                 : <CircularProgress className="loader" color="secondary"/>
            }
        </div>
    )
}