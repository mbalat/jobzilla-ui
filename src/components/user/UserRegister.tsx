import { Button, CircularProgress, Grid, TextField, Typography, Link } from "@material-ui/core";
import react, { useContext, useState } from "react"
import { useEffect } from "react";
import { useHistory } from "react-router-dom";
import AuthContext from "../../context/Auth"
import UserService from "../../services/UserService";
import IUser from "../../types/IUser";

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const authContext = useContext(AuthContext); 
    const history = useHistory();
    const [model, setModel] = useState<IUser>();
    const [active, setActive] = useState(false);
    const [errorMessage, setErrorMessage] = useState<string>();
    const [timer, setTimer] = useState(10);
    const [isLoading, setLoading] = useState<boolean>(false);

    const onSubmit = async (e: any) => {
        e.preventDefault();

        setErrorMessage("Success.");
        setActive(!active);
        
        if(!model)
            return;
        
        try {
            const result = await UserService.register(model);
            if(active === false){
                setLoading(true);
                setTimeout(() => {
                    history.push('/users/login');
                }, 5000);
            }

        } catch (error) {
            setErrorMessage(error.response.data.message);
            setLoading(false);
        }
    }

    const setModelValue = (key: string, value: any) =>
    {
        setModel(Object.assign({}, model, {[key]: value}));
    }

    return(
        <div>
            <Typography variant="h5"> Register</Typography>
            <div>
                <form className="f-job" autoComplete="off" onSubmit={onSubmit}>
                    <Grid xs={12}>
                        <TextField id="FullName" label="FullName" required onChange={e => setModelValue("FullName", e.target.value)} />
                    </Grid>
                    <Grid xs={12}>
                        <TextField id="UserName" label="UserName" required onChange={e => setModelValue("UserName", e.target.value)} />
                    </Grid>
                    <Grid xs={12}>
                        <TextField id="Password" label="Password" required type="password" onChange={e => setModelValue("Password", e.target.value)} />
                    </Grid>
                    <Grid xs={12}>
                        <TextField id="Email" label="Email" required type="email" onChange={e => setModelValue("Email", e.target.value)} />
                    </Grid>
                    <Grid xs={12}>
                        <TextField variant="outlined" id="About" label="About" fullWidth multiline rows={12} required onChange={e => setModelValue("About", e.target.value)} />
                    </Grid>
                    <div className="space"></div>
                    <Button className="w-2" variant="contained" color="primary" type="submit"> Register</Button>
                    <div className="space"></div>

                    <Link className="f-job" href="/users/login">Login </Link>

                    <div className={active ? "modal" : "disabled"}>
                    <div className="modal-content">
                        <div className="w-m">{errorMessage}</div>
                        {isLoading === false ? <div className="w-b">
                            <Button variant="contained" color="primary" type="button" onClick={() => setActive(!active)}> Close</Button>
                        </div> : <CircularProgress className="loader w-m" color="secondary"/>}
                    </div>

                </div>
                </form>
            </div>
        </div>
    )  
}