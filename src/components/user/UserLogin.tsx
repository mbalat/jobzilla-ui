import { Button, Grid, TextField, Typography, Link } from "@material-ui/core";
import react, { useContext, useState } from "react"
import { useHistory } from "react-router-dom";
import AuthContext from "../../context/Auth"
import UserService from "../../services/UserService";
import IUser from "../../types/IUser";

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const authContext = useContext(AuthContext); 
    const history = useHistory();
    const [model, setModel] = useState<IUser>();

    const onSubmit = async (e: any) => {
        e.preventDefault();

        if(!model)
            return;

        try {
            const result = await UserService.login(model);
            UserService.setToken(result.data.token);
            authContext.setAuthenticated(true);
            history.push('/jobs')   
        } catch (error) {
            console.dir(error);
        }
    }

    const setModelValue = (key: string, value: any) =>
    {
        setModel(Object.assign({}, model, {[key]: value}));
    }

    return(
        <div>
            <Typography variant="h5"> Login</Typography>
            <div className="login-form">
                <form className="center-content" autoComplete="off" onSubmit={onSubmit}>
                    <Grid xs={12}>
                        <TextField id="username" label="Username" required onChange={e => setModelValue("username", e.target.value)} />
                    </Grid>
                    <Grid xs={12}>
                        <TextField id="password" label="Password" required type="password" onChange={e => setModelValue("password", e.target.value)} />
                    </Grid>
                    <Button className="w-2" variant="contained" color="primary" type="submit"> Login</Button>
                </form>
                <div className="space"></div>
                <Link href="/users/register">Create account</Link>
            </div>
        </div>
    )  
}