import { Button, Grid, TextField, Typography } from "@material-ui/core";
import { useState } from "react";
import { useHistory, useParams } from "react-router-dom"
import UserService from "../../services/UserService";
import IUserUpdate from "../../types/IUser";

interface User{
    id: string
}

// eslint-disable-next-line import/no-anonymous-default-export
export default () =>
{
    const history = useHistory();
    const { id } = useParams<User>();
    const [model, setModel] = useState<IUserUpdate>();
    const [active, setActive] = useState(false);
    const [errorMessage, setErrorMessage] = useState<string>("You successfully changed your info.");

    const onSubmit = async (e: any) =>
    {
        e.preventDefault();

        if(!model)
        return;
        
        const result = await UserService.update(model);
        setErrorMessage("You successfully changed your info.");
        history.push('/users/profile');
            
    }
    
    const setModelValue = (key: string, value: any) =>
    {
        setModel(Object.assign({}, model, {[key]: value}));
    }

    return(
        <div>
            <Typography variant="h5">Update profile</Typography>
            <form autoComplete="off" onSubmit={onSubmit} className="f-job">
                <Grid xs={12}>
                    <TextField variant="outlined" id="Email" label="Email" type="email" onChange={e => setModelValue("Email", Number(e.target.value))} />
                </Grid>
                <Grid xs={12}>
                    <TextField variant="outlined" id="About" label="About" fullWidth multiline rows={12} onChange={e => setModelValue("About", e.target.value)} />
                </Grid>
                <div className="space"></div>
                <Button variant="contained" color="primary" type="button" onClick={() => setActive(!active)}> Submit</Button>

                <div className={active ? "modal" : "disabled"}>
                    <div className="modal-content">
                        <div className="w-m">{errorMessage}</div>
                        <div className="w-b">
                            <Button variant="contained" color="primary" type="submit" onClick={() => setActive(!active)}> Close</Button>
                        </div>
                    </div>
                    
                </div>
            </form>
        </div>
    )
}